# dofiles
Install via the `install.sh` script while inside the directiory.

```
$ ./install.sh
```


## i3 investigation

Figure out how to control:
- [x] Screen brightness
- [x] Screen resolution
- [x] Screen refresh rate
- [ ] Touchpad
- [ ] Keyboard layouts
- [ ] HiDPI on larger than 1080p resolutions
- [ ] Audio output and sound levels


Figure out how to nicely use/set (at least for work):
- [ ] Multiple screens and their resolutions and orientations
- [ ] Audio



### xrandr

`xrandr` is a utility that will be used quite a lot when working with i3, since we need to be able to control the
screens somehow and i3 doesn't give us a utility to do that. It's a good thing, because it forces me to learn `xrandr`.
Here are a few things that are important for me to remember.

#### Setting screen resolution and refresh rate

We can set the resolution and refresh rate of the monitors through xrandr. It is important that the resolution and
refresh rate that we use do actually exist. If we don't we will probably get an error message.

```bash
output=eDP # The main screen (output) name that we have on yordle

xrandr --output ${output} --mode 1920x1200
xrandr --output ${output} --mode  

# More examples
xrandr --output eDP --mode 3072x1920 --rate 120
xrandr --output eDP --mode 3072x1920 --rate 60
xrandr --output eDP --mode 3072x1920 --rate 96
xrandr --output eDP --mode 3072x1920 --rate 120

# In the first case here, the rate did not actually go to 120Hz. Not sure if this fucks with the monitor or not
xrandr --output eDP --mode 1920x1200 --rate 120

# Using a mode that doesn't exist
xrandr --output eDP --mode 1921x1201
xrandr: cannot find mode 1921x1201
```

Theoretically, we are able to create modes on our own, but to be honest, I'm not sure if this is something we want to
do really. I think that we should stick to the modes that are being advertized by the monitors themselves and then use
those.

#### Setting screen rotation

We can set the rotation of the screen through xrandr. There is the command that we can use:

```bash
output=eDP # The main screen (output) name that we have on yordle

# Rotation can be one of
rotation=inverted
rotation=right
rotation=left
rotation=normal

xrandr --output $output --rotate $rotation
```


### Terminal emulator

`i3-sensible-terminal` is essentially just a bash script that tries to find the first matching terminal emulator and
starts it up. There should be a way for me to ensure that a particular terminal emulator exists, otherwise, try to run
the default terminal (perhaps through `i3-sensible-terminal`?).

The important thing here is that it shouldn't depend on my .bashrc or .profile for example. It should assume
that i3 lives alone and it's only the i3 configuration that has actually been gotten from my dotfiles. This would avoid
faulty copy of the rest of my configuration and be able to report the issue and still work!


### DMenu

As far as I see, this is the way that i3 starts up applications. It's very much like the pop-shell, when I press the
windows key, but it looks uglier. It would be cool if I can somehow configure a place where this DMenu gets some extra
info from. For example, the `.desktop` files in `/usr/share/applications` or the way that pop-os does it.

In the config file, it's mentioned that a modern replacemet for DMenu is `rofi`. As per config file comments:

> # There also is i3-dmenu-desktop which only displays applications shipping a
> # .desktop file. It is a wrapper around dmenu, so you need that installed.
> # bindcode $mod+40 exec --no-startup-id i3-dmenu-desktop

### i3lock

This looks to be the default method of locking the user's session. Is this the optimal way? If yes, can we make it look
nice? Allow Mod+Esc to lock or Mod+L.

Autolock after inactivity period?

### Batter, power settings and power usage

The battery remaining power seems to be displayed in `i3status`. This is nice, however it's also necessary to be able
to select the different power modes easily. It might be necessary to do it through the command line, but maybe there's
a nicer way?

### Multiple monitors

It looks like it should be fairly easy to set up multiple monitors correctly in i3. However, there are a few things that
need to be figured out and properly configured in order to be able to use this on the laptops (both work and personal):

- Redo workspaces when a/multiple/all monitor/s gets disconnected (can be through a keybind)
- How to easily be able to use the same configuration file on multiple workstations given that the output names (xrandr)
  are different

### HiDPI

Figure out how to do zoom when we're on high resolutions. Currently, even on 1080p, the status bar looks very small and
is hard to read. Figure this out and use it as a part of the multiple monitor setup described above.

It looks like (now after a restart of i3) that the i3status bar does indeed get bigger if the resolution of the screen
goes down. Now that I got to 1920x1200 I can see the status line muuuuch better! Don't know what happened before though.
Perhaps I need to investigate it again if it appears. Maybe it was related to some configuration that was set.

### Splitting horizontally or vertically

Figure out how this works in the first place please :D
Are we even able to change the current orientation of windows in i3? Probably, but how?

### Container layouts

Figure those out too please


### Resizing

Also should be configured nicely

### i3status

That's the default status bar that shows the workspaces and the default information. It's an entirely separate process
and can be replaced, however it looks like a sensible default. So, either figure out how to configure it to show what
you want, or find a good enough replacement.

### Brightness and sound control (Fn buttons)

These need to be able to work. The rest, not so much necessarily, but the brightness and sound control need to work.

#### Brightness

Brightness control is done through `/sys/class/backlight/amdgpu_bl1` on yordle. I'm pretty sure that the path will
change when I use the work laptop. There must be a way that I can identify the correct path for this and be able to set
the backlight of the laptop properly. Here's how it can be done though:

```bash
backlight_path=/sys/class/backlight/amdgpu_bl1

current_brightness_level=$(cat ${backlight_path}/brightness)
max_brightness_level=$(cat ${backlight_path}/max_brightness)

# Set brightness level (requires sudo)
brightness_level=50
echo $brightness_level > ${backlight_path}/brightness

# If we go over the max_brightness_level allowed, we will receive an error as such:
root@yordle:/sys/devices/pci0000:00/0000:00:08.1/0000:73:00.0/drm/card1/card1-eDP-1/amdgpu_bl1# echo 260 > ./brightness
bash: echo: write error: Invalid argument
```

We're also able to set the brightness of a monitor through `xrandr` but this is a software level brightness set, so
it won't actually ask the monitor to change its brightness level. We don't want that, we want to control the monitor
itself.


### Input configuration (touchpad, mouse, keyboard)

Touchpad settings:
- tap to click
- natural scrolling
- more?


#### xinput

`xinput` is the Xorg utility that can control inputs (keyboards, mice and touchpads). It is able to set the properties
for the touch-to-tap and natural scrolling, acceleration of mouse movement and others. It can also enable or disable
inputs.

We will need to figure out how to properly get the correct names for the touchpads and their props. This will probably
be a bit difficult, but maybe we can try to investigate how Ubuntu/Pop-OS actually does it. Let's see if we can or not.

```bash
xinput set-prop "06CBCE44:00 06CB:CF00 Touchpad" "libinput Tapping Enabled" 1
xinput set-prop "06CBCE44:00 06CB:CF00 Touchpad" "libinput Natural Scrolling Enabled" 1
```

This can be set through configuration files or using those commands. What is the best way?

##### Resources
- https://fedoraproject.org/wiki/Input_device_configuration
- https://unix.stackexchange.com/questions/337008/activate-tap-to-click-on-touchpad
- https://askubuntu.com/questions/403113/how-do-you-enable-tap-to-click-via-command-line
- https://major.io/p/tray-icons-in-i3/
- https://wiki.archlinux.org/title/Xinput
- https://superuser.com/questions/1528211/enabling-touchpad-tapping-after-installing-dwm-on-arch-linux
- https://askubuntu.com/questions/1049965/how-do-i-find-out-which-touchpad-driver-is-being-used


### Dark theme on i3

Are we able to set the "theme" of i3? Some applications (such as google chrome, obsidian, etc) adapt to the system's
theme - can we set that to be dark in i3?


### Camera and screenshare

Figure if those things just work or not


### Suspending the laptop when lid is closed

This seems to just work, which is nice, but maybe figure out how it works? Why does it work.. etc

### Figure out the ssh agent, how would this be done through i3?



## ChatGPT answers for i3:

Good day! Let's delve into how Desktop Environments (DEs) in Linux persist input device settings and why you're seeing different behaviors between Gnome and i3.

### Why Settings Persist in Gnome but Not in i3

#### Gnome's Approach
Gnome uses a combination of the GSettings configuration system and its own background services to manage device settings. When you change settings through Gnome's Settings application, it stores these preferences using GSettings, which persists them across sessions. Gnome also has the **gnome-settings-daemon**, which applies these settings when you log in.

For instance, when you enable natural scrolling for trackpads in Gnome, this preference is saved, and gnome-settings-daemon ensures these settings are applied to any connected trackpads when you log in.

#### i3's Approach
i3 is a window manager, not a full desktop environment like Gnome. It is designed to be minimal and lightweight, leaving many configuration tasks to the user. i3 does not have a settings daemon equivalent to Gnome's gnome-settings-daemon. Therefore, it doesn't automatically apply the input device settings configured in Gnome. You need to manually configure these settings for i3, often by scripting or using tools like `xinput`.

### Persisting Settings Across Different DEs

#### System Configuration vs. DE-specific Configuration
- **System-wide Configuration:** Tools like `xinput` or configuration files in `/etc/X11/xorg.conf.d/` can set input device settings system-wide. These settings would affect any DE or window manager.
- **DE-specific Configuration:** DEs like Gnome store their settings in user-specific configuration databases (like GSettings for Gnome), which are then applied by DE-specific daemons.

### How Gnome Applies Settings to All Trackpads

Gnome uses udev rules and the capabilities of the gnome-settings-daemon to apply settings like natural scrolling to all devices of a certain type, not just individual devices. Here's a rough outline of the process:

1. **Detection:** When a new device is connected, udev (the device manager for the Linux kernel) sends a notification.
2. **Application:** gnome-settings-daemon listens for these notifications and applies the user's settings based on device type and capabilities. This is done through GSettings schemas, which abstract the hardware settings.

### Practical Steps for i3

To achieve similar behavior in i3, you'll need to manually configure your input device settings. Here’s how you can do it:

1. **Using `xinput`:**
   - List your input devices: `xinput list`
   - Find the device ID of your trackpad and set natural scrolling:
     ```bash
     xinput set-prop <device-id> "libinput Natural Scrolling Enabled" 1
     ```
   - To make this persistent, you can add the `xinput` command to your i3 configuration file (`~/.config/i3/config` or `~/.i3/config`).

2. **Using udev rules:**
   - Create a custom udev rule to set the property for all trackpads.
   - Example: Create a file `/etc/udev/rules.d/90-custom-trackpad.rules` with:
     ```
     ACTION=="add", ATTRS{name}=="*Trackpad*", RUN+="/usr/bin/xinput set-prop 'pointer:<name-of-your-device>' 'libinput Natural Scrolling Enabled' 1"
     ```
   - Replace `<name-of-your-device>` with the appropriate device name pattern.

By configuring these settings through `xinput` or udev rules, you ensure that they are applied regardless of the DE or window manager you are using. This approach aligns with the Unix philosophy of having small, modular tools that do one thing well, as opposed to relying on a comprehensive DE-specific daemon.

In summary, Gnome persists and applies settings using its own services and configuration system, while i3 relies on user-defined scripts or system-wide settings to achieve the same effect.
