-- NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = " "

-- Basic config
require("federlizer.set")

-- Set the remaps
require("federlizer.remap")

-- Then load plugins (lazy plugin)
-- Each plugin get's configured on its own, using post_config
-- Options for each plugin are gotten from opts
require("federlizer.lazy")

-- Then configure the colored pensils
require("federlizer.colors")
