
-- Show dem numbers =]
vim.opt.number = true
vim.opt.relativenumber = true

-- Tabulation
vim.opt.tabstop = 4      -- number of spaces that a \t in the file counts for
vim.opt.softtabstop = 4  -- number of spaces that a \t counts for while performing editing operations (insert mode tab in or backspace)
vim.opt.shiftwidth = 4   -- number of spaces to use for each step of (auto)indent (<< or >> operations)
vim.opt.expandtab = true -- insert spaces instead of tabs

-- Searching
vim.opt.hlsearch = true
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true -- Search case sensitive despite ignorecase if search pattern contains an upper case character

-- Window splitting
vim.opt.splitbelow = true
vim.opt.splitright = true

-- File encoding settings
vim.opt.encoding = "utf-8"
vim.opt.fileencoding = "utf-8"

-- Undo settings
vim.opt.undofile = true -- Persist undo

vim.opt.termguicolors = true

vim.opt.scrolloff = 8

vim.opt.colorcolumn = { 120 }

vim.opt.cursorline = true
vim.opt.wrap = false
