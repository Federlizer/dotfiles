vim.keymap.set("n", "<leader>h", "<cmd>:nohl<cr>")

-- Use global clipboard
vim.keymap.set("v", "<leader>y", '"+y')
vim.keymap.set("n", "<leader>p", '"+p')
vim.keymap.set("v", "<leader>p", '"+p')
vim.keymap.set("n", "<leader>P", '"+P')
vim.keymap.set("v", "<leader>P", '"+P')

-- Git configurations
vim.keymap.set("n", "<leader>gd", ":Gdiffsplit<CR>")
