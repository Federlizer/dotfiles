-- Bootstrap lazy
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

-- Load plugins
require("lazy").setup({
    {
        "hrsh7th/nvim-cmp",
        config = function()
            require("federlizer.plugin.nvim-cmp").config()
        end,
        dependencies = {
            { "hrsh7th/cmp-buffer" },
            { "hrsh7th/cmp-nvim-lsp" },
        }
    },
    {
        "nvim-telescope/telescope.nvim",
        dependencies = { "nvim-lua/plenary.nvim" },
        config = function()
            require("federlizer.plugin.telescope").config()
        end,
    },
    {
        "nvim-treesitter/nvim-treesitter",
        config = function()
            require("federlizer.plugin.nvim-treesitter").config()
        end,
    },
    {
        "catppuccin/nvim",
        name = "catppuccin",
        -- prioritize loading this first... Lazy docs said so...
        priority = 1000,
    },
    {
        "VonHeikemen/lsp-zero.nvim",
        dependencies = {
            {"neovim/nvim-lspconfig"},
            {"williamboman/mason-lspconfig.nvim"},
            {"hrsh7th/nvim-cmp"},
            {
                "williamboman/mason.nvim",
                build = function()
                    vim.cmd.MasonUpdate()
                end,
            },
        },
        config = function()
            require("federlizer.plugin.lsp-zero").config()
        end,
    },
    {
        "windwp/nvim-autopairs",
        config = function()
            require("federlizer.plugin.nvim-autopairs").config()
        end,
    },
    { "vmware-archive/salt-vim" },
    { "tpope/vim-fugitive" },
    { "nvim-tree/nvim-web-devicons" },
})
