local function config()
    -- For some reason, lazy isn't able to correctly require the plugin, so
    -- we do it manually
    require("nvim-autopairs").setup()
end

return {
    config = config,
}
