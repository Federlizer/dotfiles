local cmp = require("cmp")

local function options()
    return {
        preselect = cmp.PreselectMode.None,
        mapping = {
            ['<C-Space>'] = cmp.mapping.complete(),
            ["<CR>"] = cmp.mapping.confirm({ select = false }),
            ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
            ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
        },
        sources = cmp.config.sources({
            { name = "nvim_lsp", priority = 1000 },
            { name = "buffer",   priority = 750 },
            -- { name = "luasnip" }, -- Don't need it right now, could add later for extra snippet powerrrrr
            -- { name = "path" },
        }),
    }
end

local function config()
    local opts = options()
    cmp.setup(opts)
end

return {
    config = config,
}
