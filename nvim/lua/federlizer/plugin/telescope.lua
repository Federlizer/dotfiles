local telescope = require("telescope")
local actions = require("telescope.actions")
local builtin = require("telescope.builtin")

local function options()
    return {
        defaults = {
            mappings = {
                i = {
                    ["<C-j>"] = actions.move_selection_next,
                    ["<C-k>"] = actions.move_selection_previous,
                },
            },
        },
    }
end

local function post_config()
    vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
    vim.keymap.set("n", "<leader>fw", builtin.live_grep, {})
end

local function config()
    local opts = options()
    telescope.setup(opts)
    post_config()
end

return {
    config = config,
}

