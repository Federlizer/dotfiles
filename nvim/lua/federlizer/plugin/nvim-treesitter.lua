local function options()
    return {
        ensure_installed = {
            "rust",
            "javascript",
            "typescript",
            "c",
            "lua",
            "vim",
            "vimdoc",
            "query",
            "yaml",
        },
        sync_install = false,
        auto_install = true,

        highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
        },
    }
end

local function config()
    local opts = options()
    local treesitter = require("nvim-treesitter.configs")
    treesitter.setup(opts)
end

return {
    config = config,
}
