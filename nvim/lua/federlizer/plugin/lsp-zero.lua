local function config()
    local lsp = require("lsp-zero").preset({
        -- Why you gotta manage an external plugin!?!?
        -- We manage nvim-cmp ourselves!
        -- Fucking hell... I spent half a day debugging why the cmp dropdown
        -- wasn't working the way it was supposed to... fuck this shit!
        manage_nvim_cmp = false,
    })

    lsp.on_attach(function(client, bufnr)
        lsp.default_keymaps({buffer = bufnr})
    end)

    -- (Optional) Configure lua language server for neovim
    require("lspconfig").lua_ls.setup(lsp.nvim_lua_ls())

    lsp.setup()
end

return {
    config = config,
}
