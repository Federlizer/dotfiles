
-- 2 spaces for tabs
vim.opt.tabstop = 2      -- number of spaces that a \t in the file counts for
vim.opt.softtabstop = 2  -- number of spaces that a \t counts for while performing editing operations (insert mode tab in or backspace)
vim.opt.shiftwidth = 2   -- number of spaces to use for each step of (auto)indent (<< or >> operations)
vim.opt.expandtab = true -- insert spaces instead of tabs
