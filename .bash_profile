export EDITOR='nvim'
export VISUAL='nvim'

export PYENV_HOME=$HOME/.pyenv
export NODENV_HOME=$HOME/.nodenv

PATH=$PATH:$PYENV_HOME/bin
PATH=$PATH:$NODENV_HOME/bin

PATH=$PATH:$HOME/bin
PATH=$PATH:$HOME/code/home/bin
PATH=$PATH:$HOME/.local/bin
PATH=$PATH:$HOME/go/bin

eval "$(pyenv init -)"
eval "$(nodenv init -)"

#export PATH=$PATH

[ -e ~/.bashrc ] && source ~/.bashrc
